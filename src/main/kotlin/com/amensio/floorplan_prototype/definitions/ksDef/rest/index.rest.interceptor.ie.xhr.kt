@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION", "NESTED_CLASS_IN_EXTERNAL_INTERFACE")

package com.amensio.floorplan_prototype.definitions.ksDef.rest

import kotlin.js.*

@JsModule("rest/interceptor/ie/xhr")
external val rest_interceptor_ie_xhr: Interceptor<Any> = definedExternally
