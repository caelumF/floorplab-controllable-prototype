@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION", "NESTED_CLASS_IN_EXTERNAL_INTERFACE")
@file:JsModule("rest/mime/registry")
package com.amensio.floorplan_prototype.definitions.ksDef.rest

import kotlin.js.*

external interface MIMEConverter {
    fun read(value: String): dynamic /* Any | when.Promise<Any> */
    fun write(value: Any): dynamic /* String | when.Promise<String> */
}
external interface Registry {
    fun lookup(mimeType: String): `when`.Promise<MIMEConverter>
    fun register(mimeType: String, converter: MIMEConverter)
}
