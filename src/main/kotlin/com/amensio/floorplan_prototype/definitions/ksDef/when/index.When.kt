@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION", "NESTED_CLASS_IN_EXTERNAL_INTERFACE")
@file:JsModule("when")
package When

import kotlin.js.*
import kotlin.js.Json
import org.khronos.webgl.*
import org.w3c.dom.*
import org.w3c.dom.events.*
import org.w3c.dom.parsing.*
import org.w3c.dom.svg.*
import org.w3c.dom.url.*
import org.w3c.fetch.*
import org.w3c.files.*
import org.w3c.notifications.*
import org.w3c.performance.*
import org.w3c.workers.*
import org.w3c.xhr.*

external fun <T> attempt(f: When._.Fn0<T>): Promise<T> = definedExternally
external fun <A1, T> attempt(f: When._.Fn1<A1, T>, arg1: A1): Promise<T> = definedExternally
external fun <A1, T> attempt(f: When._.Fn1<A1, T>, arg1: Promise<A1>): Promise<T> = definedExternally
external fun <A1, A2, T> attempt(f: When._.Fn2<A1, A2, T>, arg1: A1, arg2: A2): Promise<T> = definedExternally
external fun <A1, A2, T> attempt(f: When._.Fn2<A1, A2, T>, arg1: A1, arg2: Promise<A2>): Promise<T> = definedExternally
external fun <A1, A2, T> attempt(f: When._.Fn2<A1, A2, T>, arg1: Promise<A1>, arg2: A2): Promise<T> = definedExternally
external fun <A1, A2, T> attempt(f: When._.Fn2<A1, A2, T>, arg1: Promise<A1>, arg2: Promise<A2>): Promise<T> = definedExternally
external fun <A1, A2, A3, T> attempt(f: When._.Fn3<A1, A2, A3, T>, arg1: A1, arg2: A2, arg3: A3): Promise<T> = definedExternally
external fun <A1, A2, A3, T> attempt(f: When._.Fn3<A1, A2, A3, T>, arg1: A1, arg2: A2, arg3: Promise<A3>): Promise<T> = definedExternally
external fun <A1, A2, A3, T> attempt(f: When._.Fn3<A1, A2, A3, T>, arg1: A1, arg2: Promise<A2>, arg3: A3): Promise<T> = definedExternally
external fun <A1, A2, A3, T> attempt(f: When._.Fn3<A1, A2, A3, T>, arg1: A1, arg2: Promise<A2>, arg3: Promise<A3>): Promise<T> = definedExternally
external fun <A1, A2, A3, T> attempt(f: When._.Fn3<A1, A2, A3, T>, arg1: Promise<A1>, arg2: A2, arg3: A3): Promise<T> = definedExternally
external fun <A1, A2, A3, T> attempt(f: When._.Fn3<A1, A2, A3, T>, arg1: Promise<A1>, arg2: A2, arg3: Promise<A3>): Promise<T> = definedExternally
external fun <A1, A2, A3, T> attempt(f: When._.Fn3<A1, A2, A3, T>, arg1: Promise<A1>, arg2: Promise<A2>, arg3: A3): Promise<T> = definedExternally
external fun <A1, A2, A3, T> attempt(f: When._.Fn3<A1, A2, A3, T>, arg1: Promise<A1>, arg2: Promise<A2>, arg3: Promise<A3>): Promise<T> = definedExternally
external fun <A1, A2, A3, A4, T> attempt(f: When._.Fn4<A1, A2, A3, A4, T>, arg1: A1, arg2: A2, arg3: A3, arg4: dynamic /* A4 | Promise<A4> */): Promise<T> = definedExternally
external fun <A1, A2, A3, A4, T> attempt(f: When._.Fn4<A1, A2, A3, A4, T>, arg1: A1, arg2: A2, arg3: Promise<A3>, arg4: dynamic /* A4 | Promise<A4> */): Promise<T> = definedExternally
external fun <A1, A2, A3, A4, T> attempt(f: When._.Fn4<A1, A2, A3, A4, T>, arg1: A1, arg2: Promise<A2>, arg3: A3, arg4: dynamic /* A4 | Promise<A4> */): Promise<T> = definedExternally
external fun <A1, A2, A3, A4, T> attempt(f: When._.Fn4<A1, A2, A3, A4, T>, arg1: A1, arg2: Promise<A2>, arg3: Promise<A3>, arg4: dynamic /* A4 | Promise<A4> */): Promise<T> = definedExternally
external fun <A1, A2, A3, A4, T> attempt(f: When._.Fn4<A1, A2, A3, A4, T>, arg1: Promise<A1>, arg2: A2, arg3: A3, arg4: dynamic /* A4 | Promise<A4> */): Promise<T> = definedExternally
external fun <A1, A2, A3, A4, T> attempt(f: When._.Fn4<A1, A2, A3, A4, T>, arg1: Promise<A1>, arg2: A2, arg3: Promise<A3>, arg4: dynamic /* A4 | Promise<A4> */): Promise<T> = definedExternally
external fun <A1, A2, A3, A4, T> attempt(f: When._.Fn4<A1, A2, A3, A4, T>, arg1: Promise<A1>, arg2: Promise<A2>, arg3: A3, arg4: dynamic /* A4 | Promise<A4> */): Promise<T> = definedExternally
external fun <A1, A2, A3, A4, T> attempt(f: When._.Fn4<A1, A2, A3, A4, T>, arg1: Promise<A1>, arg2: Promise<A2>, arg3: Promise<A3>, arg4: dynamic /* A4 | Promise<A4> */): Promise<T> = definedExternally
external fun <A1, A2, A3, A4, A5, T> attempt(f: When._.Fn5<A1, A2, A3, A4, A5, T>, arg1: A1, arg2: A2, arg3: A3, arg4: dynamic /* A4 | Promise<A4> */, arg5: dynamic /* A5 | Promise<A5> */): Promise<T> = definedExternally
external fun <A1, A2, A3, A4, A5, T> attempt(f: When._.Fn5<A1, A2, A3, A4, A5, T>, arg1: A1, arg2: A2, arg3: Promise<A3>, arg4: dynamic /* A4 | Promise<A4> */, arg5: dynamic /* A5 | Promise<A5> */): Promise<T> = definedExternally
external fun <A1, A2, A3, A4, A5, T> attempt(f: When._.Fn5<A1, A2, A3, A4, A5, T>, arg1: A1, arg2: Promise<A2>, arg3: A3, arg4: dynamic /* A4 | Promise<A4> */, arg5: dynamic /* A5 | Promise<A5> */): Promise<T> = definedExternally
external fun <A1, A2, A3, A4, A5, T> attempt(f: When._.Fn5<A1, A2, A3, A4, A5, T>, arg1: A1, arg2: Promise<A2>, arg3: Promise<A3>, arg4: dynamic /* A4 | Promise<A4> */, arg5: dynamic /* A5 | Promise<A5> */): Promise<T> = definedExternally
external fun <A1, A2, A3, A4, A5, T> attempt(f: When._.Fn5<A1, A2, A3, A4, A5, T>, arg1: Promise<A1>, arg2: A2, arg3: A3, arg4: dynamic /* A4 | Promise<A4> */, arg5: dynamic /* A5 | Promise<A5> */): Promise<T> = definedExternally
external fun <A1, A2, A3, A4, A5, T> attempt(f: When._.Fn5<A1, A2, A3, A4, A5, T>, arg1: Promise<A1>, arg2: A2, arg3: Promise<A3>, arg4: dynamic /* A4 | Promise<A4> */, arg5: dynamic /* A5 | Promise<A5> */): Promise<T> = definedExternally
external fun <A1, A2, A3, A4, A5, T> attempt(f: When._.Fn5<A1, A2, A3, A4, A5, T>, arg1: Promise<A1>, arg2: Promise<A2>, arg3: A3, arg4: dynamic /* A4 | Promise<A4> */, arg5: dynamic /* A5 | Promise<A5> */): Promise<T> = definedExternally
external fun <A1, A2, A3, A4, A5, T> attempt(f: When._.Fn5<A1, A2, A3, A4, A5, T>, arg1: Promise<A1>, arg2: Promise<A2>, arg3: Promise<A3>, arg4: dynamic /* A4 | Promise<A4> */, arg5: dynamic /* A5 | Promise<A5> */): Promise<T> = definedExternally
external fun <T> lift(f: When._.Fn0<T>): When._.LiftedFn0<T> = definedExternally
external fun <A1, T> lift(f: When._.Fn1<A1, T>): When._.LiftedFn1<A1, T> = definedExternally
external fun <A1, A2, T> lift(f: When._.Fn2<A1, A2, T>): When._.LiftedFn2<A1, A2, T> = definedExternally
external fun <A1, A2, A3, T> lift(f: When._.Fn3<A1, A2, A3, T>): When._.LiftedFn3<A1, A2, A3, T> = definedExternally
external fun <A1, A2, A3, A4, T> lift(f: When._.Fn4<A1, A2, A3, A4, T>): When._.LiftedFn4<A1, A2, A3, A4, T> = definedExternally
external fun <A1, A2, A3, A4, A5, T> lift(f: When._.Fn5<A1, A2, A3, A4, A5, T>): When._.LiftedFn5<A1, A2, A3, A4, A5, T> = definedExternally
external fun <T> promise(resolver: (resolve: (value: T) -> Unit, reject: (reason: Any) -> Unit) -> Unit): Promise<T> = definedExternally
external fun <T> reject(reason: Any): Promise<T> = definedExternally
external fun <T> all(promisesOrValues: Array<Any>): Promise<T> = definedExternally
external fun <T> map(promisesOrValues: Array<Any>, mapFunc: (value: Any, index: Number? /*= null*/) -> Any): Promise<T> = definedExternally
external fun <T> reduce(promisesOrValues: Array<Any>, reduceFunc: (reduction: T, value: Any, index: Number? /*= null*/) -> dynamic /* T | Promise<T> */, initialValue: T): Promise<T> = definedExternally
external fun <T> reduceRight(promisesOrValues: Array<Any>, reduceFunc: (reduction: T, value: Any, index: Number? /*= null*/) -> dynamic /* T | Promise<T> */, initialValue: T): Promise<T> = definedExternally
external interface Descriptor<T> {
    var state: String
    var value: T? get() = definedExternally; set(value) = definedExternally
    var reason: Any? get() = definedExternally; set(value) = definedExternally
}
external fun <T> settle(promisesOrValues: Array<Any>): Promise<Array<Descriptor<T>>> = definedExternally
external fun <U> iterate(f: (seed: U) -> dynamic /* U | Promise<U> */, predicate: (value: U) -> Boolean, handler: (value: U) -> dynamic /* Unit | Promise<Any> */, seed: U): Promise<U> = definedExternally
external fun <U> iterate(f: (seed: U) -> dynamic /* U | Promise<U> */, predicate: (value: U) -> Boolean, handler: (value: U) -> dynamic /* Unit | Promise<Any> */, seed: Promise<U>): Promise<U> = definedExternally
external fun <T, U> unfold(unspool: (seed: U) -> dynamic /* dynamic /* JsTuple<dynamic /* T | Promise<T> */, dynamic /* U | Promise<U> */> */ | Promise<dynamic /* JsTuple<dynamic /* T | Promise<T> */, dynamic /* U | Promise<U> */> */> */, predicate: (value: U) -> dynamic /* Boolean | Promise<Boolean> */, handler: (value: T) -> dynamic /* Unit | Promise<Any> */, seed: U): Promise<Unit> = definedExternally
external fun <T, U> unfold(unspool: (seed: U) -> dynamic /* dynamic /* JsTuple<dynamic /* T | Promise<T> */, dynamic /* U | Promise<U> */> */ | Promise<dynamic /* JsTuple<dynamic /* T | Promise<T> */, dynamic /* U | Promise<U> */> */> */, predicate: (value: U) -> dynamic /* Boolean | Promise<Boolean> */, handler: (value: T) -> dynamic /* Unit | Promise<Any> */, seed: Promise<U>): Promise<Unit> = definedExternally
external fun <T> defer(): Deferred<T> = definedExternally
external fun <T> join(vararg promises: Promise<T>): Promise<Array<T>> = definedExternally
external fun <T> join(vararg promises: Any): Promise<Array<T>> = definedExternally
external fun <T> resolve(promise: Promise<T>): Promise<T> = definedExternally
external fun <T> resolve(foreign: Thenable<T>): Promise<T> = definedExternally
external fun <T> resolve(value: T? = definedExternally /* null */): Promise<T> = definedExternally
external interface Deferred<T> {
    fun notify(update: Any)
    var promise: Promise<T>
    fun reject(reason: Any)
    fun resolve(value: T? = definedExternally /* null */)
    fun resolve(value: Promise<T>? = definedExternally /* null */)
    fun resolve()
}
external interface Promise<T> {
    fun <U> catch(onRejected: ((reason: Any) -> dynamic /* U | Promise<U> */)? = definedExternally /* null */): Promise<U>
    fun <U> catch(filter: (reason: Any) -> Boolean, onRejected: ((reason: Any) -> dynamic /* U | Promise<U> */)? = definedExternally /* null */): Promise<U>
    fun <U> catch(exceptionType: Any, onRejected: ((reason: Any) -> dynamic /* U | Promise<U> */)? = definedExternally /* null */): Promise<U>
    fun finally(onFulfilledOrRejected: Function<*>): Promise<T>
    fun ensure(onFulfilledOrRejected: Function<*>): Promise<T>
    fun inspect(): Snapshot<T>
    fun <U> yield(value: U): Promise<U>
    fun <U> yield(value: Promise<U>): Promise<U>
    fun `else`(value: T): Promise<T>
    fun orElse(value: T): Promise<T>
    fun tap(onFulfilledSideEffect: (value: T) -> Unit): Promise<T>
    fun delay(milliseconds: Number): Promise<T>
    fun timeout(milliseconds: Number, reason: Any? = definedExternally /* null */): Promise<T>
    fun with(thisArg: Any): Promise<T>
    fun withThis(thisArg: Any): Promise<T>
    fun <U> otherwise(onRejected: ((reason: Any) -> dynamic /* U | Promise<U> */)? = definedExternally /* null */): Promise<U>
    fun <U> otherwise(predicate: (reason: Any) -> Boolean, onRejected: ((reason: Any) -> dynamic /* U | Promise<U> */)? = definedExternally /* null */): Promise<U>
    fun <U> otherwise(exceptionType: Any, onRejected: ((reason: Any) -> dynamic /* U | Promise<U> */)? = definedExternally /* null */): Promise<U>
    fun <U> then(onFulfilled: (value: T) -> dynamic /* U | Promise<U> */, onRejected: ((reason: Any) -> dynamic /* U | Promise<U> */)? = definedExternally /* null */, onProgress: ((update: Any) -> Unit)? = definedExternally /* null */): Promise<U>
    fun <T> spread(onFulfilled: When._.Fn0<dynamic /* T | Promise<T> */>): Promise<T>
    fun <A1, T> spread(onFulfilled: When._.Fn1<A1, dynamic /* T | Promise<T> */>): Promise<T>
    fun <A1, A2, T> spread(onFulfilled: When._.Fn2<A1, A2, dynamic /* T | Promise<T> */>): Promise<T>
    fun <A1, A2, A3, T> spread(onFulfilled: When._.Fn3<A1, A2, A3, dynamic /* T | Promise<T> */>): Promise<T>
    fun <A1, A2, A3, A4, T> spread(onFulfilled: When._.Fn4<A1, A2, A3, A4, dynamic /* T | Promise<T> */>): Promise<T>
    fun <A1, A2, A3, A4, A5, T> spread(onFulfilled: When._.Fn5<A1, A2, A3, A4, A5, dynamic /* T | Promise<T> */>): Promise<T>
    fun <U> done(onFulfilled: ((value: T) -> Unit)? = definedExternally /* null */, onRejected: ((reason: Any) -> Unit)? = definedExternally /* null */)
    fun <U, V> fold(combine: (value1: T, value2: V) -> dynamic /* U | Promise<U> */, value2: V): Promise<U>
    fun <U, V> fold(combine: (value1: T, value2: V) -> dynamic /* U | Promise<U> */, value2: Promise<V>): Promise<U>
}
external interface Thenable<T> {
    fun <U> then(onFulfilled: (value: T) -> U, onRejected: ((reason: Any) -> U)? = definedExternally /* null */): Thenable<U>
}
external interface Snapshot<T> {
    var state: String
    var value: T? get() = definedExternally; set(value) = definedExternally
    var reason: Any? get() = definedExternally; set(value) = definedExternally
}
