@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION", "NESTED_CLASS_IN_EXTERNAL_INTERFACE")
@file:JsModule("rest/interceptor/oAuth")
package com.amensio.floorplan_prototype.definitions.ksDef.rest

import kotlin.js.*

external interface DismissWindow {
    @nativeInvoke
    operator fun invoke()
}
external interface Config {
    var token: String? get() = definedExternally; set(value) = definedExternally
    var clientId: String? get() = definedExternally; set(value) = definedExternally
    var scope: String? get() = definedExternally; set(value) = definedExternally
    var authorizationUrl: String? get() = definedExternally; set(value) = definedExternally
    var redirectUrl: String? get() = definedExternally; set(value) = definedExternally
    var windowStrategy: ((url: String) -> DismissWindow)? get() = definedExternally; set(value) = definedExternally
    var oAuthCallback: ((hash: String) -> Unit)? get() = definedExternally; set(value) = definedExternally
    var oAuthCallbackName: String? get() = definedExternally; set(value) = definedExternally
}
