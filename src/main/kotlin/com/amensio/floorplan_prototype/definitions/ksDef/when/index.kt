@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION", "NESTED_CLASS_IN_EXTERNAL_INTERFACE")

import kotlin.js.*
import kotlin.js.Json
import org.khronos.webgl.*
import org.w3c.dom.*
import org.w3c.dom.events.*
import org.w3c.dom.parsing.*
import org.w3c.dom.svg.*
import org.w3c.dom.url.*
import org.w3c.fetch.*
import org.w3c.files.*
import org.w3c.notifications.*
import org.w3c.performance.*
import org.w3c.workers.*
import org.w3c.xhr.*

@JsModule("when")
external fun <T> When(value: When.Promise<T>): When.Promise<T> = definedExternally
@JsModule("when")
external fun <T> When(value: When.Thenable<T>): When.Promise<T> = definedExternally
@JsModule("when")
external fun <T> When(value: T): When.Promise<T> = definedExternally
@JsModule("when")
external fun <T, U> When(value: When.Promise<T>, transform: (val: T) -> U): When.Promise<U> = definedExternally
@JsModule("when")
external fun <T, U> When(value: When.Thenable<T>, transform: (val: T) -> U): When.Promise<U> = definedExternally
@JsModule("when")
external fun <T, U> When(value: T, transform: (val: T) -> U): When.Promise<U> = definedExternally
