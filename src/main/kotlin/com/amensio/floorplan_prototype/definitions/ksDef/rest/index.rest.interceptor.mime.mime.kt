@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION", "NESTED_CLASS_IN_EXTERNAL_INTERFACE")
@file:JsModule("rest/interceptor/mime")
package com.amensio.floorplan_prototype.definitions.ksDef.rest

import kotlin.js.*

external interface Config {
    var mime: String? get() = definedExternally; set(value) = definedExternally
    var accept: String? get() = definedExternally; set(value) = definedExternally
    var registry: Registry? get() = definedExternally; set(value) = definedExternally
    var permissive: Boolean? get() = definedExternally; set(value) = definedExternally
}
