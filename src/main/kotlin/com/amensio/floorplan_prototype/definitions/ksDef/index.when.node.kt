@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION", "NESTED_CLASS_IN_EXTERNAL_INTERFACE")
@file:JsModule("when/node")
package when.node

import kotlin.js.*
import kotlin.js.Json
import org.khronos.webgl.*
import org.w3c.dom.*
import org.w3c.dom.events.*
import org.w3c.dom.parsing.*
import org.w3c.dom.svg.*
import org.w3c.dom.url.*
import org.w3c.fetch.*
import org.w3c.files.*
import org.w3c.notifications.*
import org.w3c.performance.*
import org.w3c.workers.*
import org.w3c.xhr.*

external fun <T> lift(fn: When._.NodeFn0<T>): When._.LiftedFn0<T> = definedExternally
external fun <A1, T> lift(fn: When._.NodeFn1<A1, T>): When._.LiftedFn1<A1, T> = definedExternally
external fun <A1, A2, T> lift(fn: When._.NodeFn2<A1, A2, T>): When._.LiftedFn2<A1, A2, T> = definedExternally
external fun <A1, A2, A3, T> lift(fn: When._.NodeFn3<A1, A2, A3, T>): When._.LiftedFn3<A1, A2, A3, T> = definedExternally
external fun <A1, A2, A3, A4, T> lift(fn: When._.NodeFn4<A1, A2, A3, A4, T>): When._.LiftedFn4<A1, A2, A3, A4, T> = definedExternally
external fun <A1, A2, A3, A4, A5, T> lift(fn: When._.NodeFn5<A1, A2, A3, A4, A5, T>): When._.LiftedFn5<A1, A2, A3, A4, A5, T> = definedExternally
external fun <T> call(fn: When._.NodeFn0<T>): When.Promise<T> = definedExternally
external fun <A1, T> call(fn: When._.NodeFn1<A1, T>, arg1: A1): When.Promise<T> = definedExternally
external fun <A1, T> call(fn: When._.NodeFn1<A1, T>, arg1: When.Promise<A1>): When.Promise<T> = definedExternally
external fun <A1, A2, T> call(fn: When._.NodeFn2<A1, A2, T>, arg1: A1, arg2: A2): When.Promise<T> = definedExternally
external fun <A1, A2, T> call(fn: When._.NodeFn2<A1, A2, T>, arg1: A1, arg2: When.Promise<A2>): When.Promise<T> = definedExternally
external fun <A1, A2, T> call(fn: When._.NodeFn2<A1, A2, T>, arg1: When.Promise<A1>, arg2: A2): When.Promise<T> = definedExternally
external fun <A1, A2, T> call(fn: When._.NodeFn2<A1, A2, T>, arg1: When.Promise<A1>, arg2: When.Promise<A2>): When.Promise<T> = definedExternally
external fun <A1, A2, A3, T> call(fn: When._.NodeFn3<A1, A2, A3, T>, arg1: A1, arg2: A2, arg3: A3): When.Promise<T> = definedExternally
external fun <A1, A2, A3, T> call(fn: When._.NodeFn3<A1, A2, A3, T>, arg1: A1, arg2: A2, arg3: When.Promise<A3>): When.Promise<T> = definedExternally
external fun <A1, A2, A3, T> call(fn: When._.NodeFn3<A1, A2, A3, T>, arg1: A1, arg2: When.Promise<A2>, arg3: A3): When.Promise<T> = definedExternally
external fun <A1, A2, A3, T> call(fn: When._.NodeFn3<A1, A2, A3, T>, arg1: A1, arg2: When.Promise<A2>, arg3: When.Promise<A3>): When.Promise<T> = definedExternally
external fun <A1, A2, A3, T> call(fn: When._.NodeFn3<A1, A2, A3, T>, arg1: When.Promise<A1>, arg2: A2, arg3: A3): When.Promise<T> = definedExternally
external fun <A1, A2, A3, T> call(fn: When._.NodeFn3<A1, A2, A3, T>, arg1: When.Promise<A1>, arg2: A2, arg3: When.Promise<A3>): When.Promise<T> = definedExternally
external fun <A1, A2, A3, T> call(fn: When._.NodeFn3<A1, A2, A3, T>, arg1: When.Promise<A1>, arg2: When.Promise<A2>, arg3: A3): When.Promise<T> = definedExternally
external fun <A1, A2, A3, T> call(fn: When._.NodeFn3<A1, A2, A3, T>, arg1: When.Promise<A1>, arg2: When.Promise<A2>, arg3: When.Promise<A3>): When.Promise<T> = definedExternally
external fun <A1, A2, A3, A4, T> call(fn: When._.NodeFn4<A1, A2, A3, A4, T>, arg1: A1, arg2: A2, arg3: A3, arg4: dynamic /* A4 | When.Promise<A4> */): When.Promise<T> = definedExternally
external fun <A1, A2, A3, A4, T> call(fn: When._.NodeFn4<A1, A2, A3, A4, T>, arg1: A1, arg2: A2, arg3: When.Promise<A3>, arg4: dynamic /* A4 | When.Promise<A4> */): When.Promise<T> = definedExternally
external fun <A1, A2, A3, A4, T> call(fn: When._.NodeFn4<A1, A2, A3, A4, T>, arg1: A1, arg2: When.Promise<A2>, arg3: A3, arg4: dynamic /* A4 | When.Promise<A4> */): When.Promise<T> = definedExternally
external fun <A1, A2, A3, A4, T> call(fn: When._.NodeFn4<A1, A2, A3, A4, T>, arg1: A1, arg2: When.Promise<A2>, arg3: When.Promise<A3>, arg4: dynamic /* A4 | When.Promise<A4> */): When.Promise<T> = definedExternally
external fun <A1, A2, A3, A4, T> call(fn: When._.NodeFn4<A1, A2, A3, A4, T>, arg1: When.Promise<A1>, arg2: A2, arg3: A3, arg4: dynamic /* A4 | When.Promise<A4> */): When.Promise<T> = definedExternally
external fun <A1, A2, A3, A4, T> call(fn: When._.NodeFn4<A1, A2, A3, A4, T>, arg1: When.Promise<A1>, arg2: A2, arg3: When.Promise<A3>, arg4: dynamic /* A4 | When.Promise<A4> */): When.Promise<T> = definedExternally
external fun <A1, A2, A3, A4, T> call(fn: When._.NodeFn4<A1, A2, A3, A4, T>, arg1: When.Promise<A1>, arg2: When.Promise<A2>, arg3: A3, arg4: dynamic /* A4 | When.Promise<A4> */): When.Promise<T> = definedExternally
external fun <A1, A2, A3, A4, T> call(fn: When._.NodeFn4<A1, A2, A3, A4, T>, arg1: When.Promise<A1>, arg2: When.Promise<A2>, arg3: When.Promise<A3>, arg4: dynamic /* A4 | When.Promise<A4> */): When.Promise<T> = definedExternally
external fun <A1, A2, A3, A4, A5, T> call(fn: When._.NodeFn5<A1, A2, A3, A4, A5, T>, arg1: A1, arg2: A2, arg3: A3, arg4: dynamic /* A4 | When.Promise<A4> */, arg5: dynamic /* A5 | When.Promise<A5> */): When.Promise<T> = definedExternally
external fun <A1, A2, A3, A4, A5, T> call(fn: When._.NodeFn5<A1, A2, A3, A4, A5, T>, arg1: A1, arg2: A2, arg3: When.Promise<A3>, arg4: dynamic /* A4 | When.Promise<A4> */, arg5: dynamic /* A5 | When.Promise<A5> */): When.Promise<T> = definedExternally
external fun <A1, A2, A3, A4, A5, T> call(fn: When._.NodeFn5<A1, A2, A3, A4, A5, T>, arg1: A1, arg2: When.Promise<A2>, arg3: A3, arg4: dynamic /* A4 | When.Promise<A4> */, arg5: dynamic /* A5 | When.Promise<A5> */): When.Promise<T> = definedExternally
external fun <A1, A2, A3, A4, A5, T> call(fn: When._.NodeFn5<A1, A2, A3, A4, A5, T>, arg1: A1, arg2: When.Promise<A2>, arg3: When.Promise<A3>, arg4: dynamic /* A4 | When.Promise<A4> */, arg5: dynamic /* A5 | When.Promise<A5> */): When.Promise<T> = definedExternally
external fun <A1, A2, A3, A4, A5, T> call(fn: When._.NodeFn5<A1, A2, A3, A4, A5, T>, arg1: When.Promise<A1>, arg2: A2, arg3: A3, arg4: dynamic /* A4 | When.Promise<A4> */, arg5: dynamic /* A5 | When.Promise<A5> */): When.Promise<T> = definedExternally
external fun <A1, A2, A3, A4, A5, T> call(fn: When._.NodeFn5<A1, A2, A3, A4, A5, T>, arg1: When.Promise<A1>, arg2: A2, arg3: When.Promise<A3>, arg4: dynamic /* A4 | When.Promise<A4> */, arg5: dynamic /* A5 | When.Promise<A5> */): When.Promise<T> = definedExternally
external fun <A1, A2, A3, A4, A5, T> call(fn: When._.NodeFn5<A1, A2, A3, A4, A5, T>, arg1: When.Promise<A1>, arg2: When.Promise<A2>, arg3: A3, arg4: dynamic /* A4 | When.Promise<A4> */, arg5: dynamic /* A5 | When.Promise<A5> */): When.Promise<T> = definedExternally
external fun <A1, A2, A3, A4, A5, T> call(fn: When._.NodeFn5<A1, A2, A3, A4, A5, T>, arg1: When.Promise<A1>, arg2: When.Promise<A2>, arg3: When.Promise<A3>, arg4: dynamic /* A4 | When.Promise<A4> */, arg5: dynamic /* A5 | When.Promise<A5> */): When.Promise<T> = definedExternally
external fun <T> apply(fn: When._.NodeFn0<T>, args: IArguments): When.Promise<T> = definedExternally
external fun <T> apply(fn: When._.NodeFn0<T>, args: Array<Any>): When.Promise<T> = definedExternally
external fun <T> apply(fn: When._.NodeFn1<Any, T>, args: IArguments): When.Promise<T> = definedExternally
external fun <T> apply(fn: When._.NodeFn1<Any, T>, args: Array<Any>): When.Promise<T> = definedExternally
external fun <T> apply(fn: When._.NodeFn2<Any, Any, T>, args: IArguments): When.Promise<T> = definedExternally
external fun <T> apply(fn: When._.NodeFn2<Any, Any, T>, args: Array<Any>): When.Promise<T> = definedExternally
external fun <T> apply(fn: When._.NodeFn3<Any, Any, Any, T>, args: IArguments): When.Promise<T> = definedExternally
external fun <T> apply(fn: When._.NodeFn3<Any, Any, Any, T>, args: Array<Any>): When.Promise<T> = definedExternally
external fun <T> apply(fn: When._.NodeFn4<Any, Any, Any, Any, T>, args: IArguments): When.Promise<T> = definedExternally
external fun <T> apply(fn: When._.NodeFn4<Any, Any, Any, Any, T>, args: Array<Any>): When.Promise<T> = definedExternally
external fun <T> apply(fn: When._.NodeFn5<Any, Any, Any, Any, Any, T>, args: IArguments): When.Promise<T> = definedExternally
external fun <T> apply(fn: When._.NodeFn5<Any, Any, Any, Any, Any, T>, args: Array<Any>): When.Promise<T> = definedExternally
external fun liftAll(srcApi: Any, transform: ((destApi: Any, liftedFunc: Function<*>, name: String) -> Any)? = definedExternally /* null */, destApi: Any? = definedExternally /* null */): Any = definedExternally
external fun <TArg> liftCallback(callback: (err: Any, arg: TArg) -> Unit): (value: When.Promise<TArg>) -> When.Promise<TArg> = definedExternally
external fun <TArg> bindCallback(arg: When.Promise<TArg>, callback: (err: Any, arg: TArg) -> Unit): When.Promise<TArg> = definedExternally
external interface Resolver<T> {
    fun reject(reason: Any)
    fun resolve(value: T? = definedExternally /* null */)
    fun resolve(value: When.Promise<T>? = definedExternally /* null */)
    fun resolve()
}
external fun <TArg> createCallback(resolver: Resolver<TArg>): (err: Any, arg: TArg) -> Unit = definedExternally
