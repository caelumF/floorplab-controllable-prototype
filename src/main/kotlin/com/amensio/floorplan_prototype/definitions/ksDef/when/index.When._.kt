@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION", "NESTED_CLASS_IN_EXTERNAL_INTERFACE")
@file:[JsModule("when") JsQualifier("_")]
package When._

import kotlin.js.*
import kotlin.js.Json
import org.khronos.webgl.*
import org.w3c.dom.*
import org.w3c.dom.events.*
import org.w3c.dom.parsing.*
import org.w3c.dom.svg.*
import org.w3c.dom.url.*
import org.w3c.fetch.*
import org.w3c.files.*
import org.w3c.notifications.*
import org.w3c.performance.*
import org.w3c.workers.*
import org.w3c.xhr.*

external interface Fn0<T> {
    @nativeInvoke
    operator fun invoke(): T
}
external interface Fn1<A1, T> {
    @nativeInvoke
    operator fun invoke(a1: A1): T
}
external interface Fn2<A1, A2, T> {
    @nativeInvoke
    operator fun invoke(a1: A1, a2: A2): T
}
external interface Fn3<A1, A2, A3, T> {
    @nativeInvoke
    operator fun invoke(a1: A1, a2: A2, a3: A3): T
}
external interface Fn4<A1, A2, A3, A4, T> {
    @nativeInvoke
    operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4): T
}
external interface Fn5<A1, A2, A3, A4, A5, T> {
    @nativeInvoke
    operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5): T
}
external interface Fn6<A1, A2, A3, A4, A5, A6, T> {
    @nativeInvoke
    operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6): T
}
external interface LiftedFn0<T> : Fn0<When.Promise<T>>
external interface LiftedFn1<A1, T> : Fn1<dynamic /* A1 | When.Promise<A1> */, When.Promise<T>>
external interface LiftedFn2<A1, A2, T> : Fn2<dynamic /* A1 | When.Promise<A1> */, dynamic /* A2 | When.Promise<A2> */, When.Promise<T>>
external interface LiftedFn3<A1, A2, A3, T> : Fn3<dynamic /* A1 | When.Promise<A1> */, dynamic /* A2 | When.Promise<A2> */, dynamic /* A3 | When.Promise<A3> */, When.Promise<T>>
external interface LiftedFn4<A1, A2, A3, A4, T> : Fn4<dynamic /* A1 | When.Promise<A1> */, dynamic /* A2 | When.Promise<A2> */, dynamic /* A3 | When.Promise<A3> */, dynamic /* A4 | When.Promise<A4> */, When.Promise<T>>
external interface LiftedFn5<A1, A2, A3, A4, A5, T> : Fn5<dynamic /* A1 | When.Promise<A1> */, dynamic /* A2 | When.Promise<A2> */, dynamic /* A3 | When.Promise<A3> */, dynamic /* A4 | When.Promise<A4> */, dynamic /* A5 | When.Promise<A5> */, When.Promise<T>>
external interface NodeCallback<T> {
    @nativeInvoke
    operator fun invoke(err: Any, result: T)
}
external interface NodeFn0<T> : Fn1<NodeCallback<T>, Unit>
external interface NodeFn1<A1, T> : Fn2<A1, NodeCallback<T>, Unit>
external interface NodeFn2<A1, A2, T> : Fn3<A1, A2, NodeCallback<T>, Unit>
external interface NodeFn3<A1, A2, A3, T> : Fn4<A1, A2, A3, NodeCallback<T>, Unit>
external interface NodeFn4<A1, A2, A3, A4, T> : Fn5<A1, A2, A3, A4, NodeCallback<T>, Unit>
external interface NodeFn5<A1, A2, A3, A4, A5, T> : Fn6<A1, A2, A3, A4, A5, NodeCallback<T>, Unit>
