@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION", "NESTED_CLASS_IN_EXTERNAL_INTERFACE")
@file:JsModule("rest/interceptor/jsonp")
package com.amensio.floorplan_prototype.definitions.ksDef.rest

import kotlin.js.*

external interface `T$0` {
    var param: String? get() = definedExternally; set(value) = definedExternally
    var prefix: String? get() = definedExternally; set(value) = definedExternally
    var name: String? get() = definedExternally; set(value) = definedExternally
}
external interface Config {
    var callback: `T$0`? get() = definedExternally; set(value) = definedExternally
}
