@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION", "NESTED_CLASS_IN_EXTERNAL_INTERFACE")
@file:JsModule("rest/interceptor")
package com.amensio.floorplan_prototype.definitions.ksDef.rest

import kotlin.js.*

external interface Config<T, U> {
    var init: ((config: T) -> U)? get() = definedExternally; set(value) = definedExternally
    var request: ((request: Request, config: U, meta: Meta) -> dynamic /* com.amensio.floorplan_prototype.definitions.ksDef.rest.restio.floorplan_prototype.definitions.ksDef.rest.Request | when.Promise<com.amensio.floorplan_prototype.definitions.ksDef.rest.restio.floorplan_prototype.definitions.ksDef.rest.Request> */)? get() = definedExternally; set(value) = definedExternally
    var response: ((response: Response, config: U, meta: Meta) -> dynamic /* com.amensio.floorplan_prototype.definitions.ksDef.rest.restio.floorplan_prototype.definitions.ksDef.rest.Response | when.Promise<com.amensio.floorplan_prototype.definitions.ksDef.rest.restio.floorplan_prototype.definitions.ksDef.rest.Response> */)? get() = definedExternally; set(value) = definedExternally
    var success: ((response: Response, config: U, meta: Meta) -> dynamic /* com.amensio.floorplan_prototype.definitions.ksDef.rest.restio.floorplan_prototype.definitions.ksDef.rest.Response | when.Promise<com.amensio.floorplan_prototype.definitions.ksDef.rest.restio.floorplan_prototype.definitions.ksDef.rest.Response> */)? get() = definedExternally; set(value) = definedExternally
    var error: ((response: Response, config: U, meta: Meta) -> dynamic /* com.amensio.floorplan_prototype.definitions.ksDef.rest.restio.floorplan_prototype.definitions.ksDef.rest.Response | when.Promise<com.amensio.floorplan_prototype.definitions.ksDef.rest.restio.floorplan_prototype.definitions.ksDef.rest.Response> */)? get() = definedExternally; set(value) = definedExternally
}
