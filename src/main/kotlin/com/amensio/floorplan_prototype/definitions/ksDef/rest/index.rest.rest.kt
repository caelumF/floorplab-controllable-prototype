@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION", "NESTED_CLASS_IN_EXTERNAL_INTERFACE")
@file:JsModule("rest")

package com.amensio.floorplan_prototype.definitions.ksDef.rest

import kotlin.js.*

external fun setDefaultClient(client: Client): Unit = definedExternally
external fun getDefaultClient(): Client = definedExternally
external fun resetDefaultClient(): Unit = definedExternally
external fun <T> wrap(interceptor: Interceptor<T>, config: T? = definedExternally /* null */): Client = definedExternally
external interface Request {
    var method: String? get() = definedExternally; set(value) = definedExternally
    var path: String? get() = definedExternally; set(value) = definedExternally
    var params: Any? get() = definedExternally; set(value) = definedExternally
    var headers: Any? get() = definedExternally; set(value) = definedExternally
    var entity: Any? get() = definedExternally; set(value) = definedExternally
    var canceled: Boolean? get() = definedExternally; set(value) = definedExternally
    var cancel: (() -> Unit)? get() = definedExternally; set(value) = definedExternally
    var originator: ((request: Request? /*= null*/) -> ResponsePromise)? get() = definedExternally; set(value) = definedExternally
}

external interface Status {
    var code: Number
    var text: String? get() = definedExternally; set(value) = definedExternally
}

external interface Headers {
    @nativeGetter
    operator fun get(index: String): Any?

    @nativeSetter
    operator fun set(index: String, value: Any)
}

external interface Response {
    var request: Request
    var raw: Any
    var status: Status
    var headers: Headers
    var entity: Any
}

external interface ResponsePromise : when.Promise<Response> {
    fun entity(): `when`.Promise<Any>
    fun status(): `when`.Promise<Number>
    fun headers(): ` when`.Promise<Headers>
    fun header(headerName: String): when.Promise<Any>
}

external interface Interceptor<T> {
    @nativeInvoke
    operator fun invoke(parent: Client? = definedExternally /* null */, config: T? = definedExternally /* null */): Client
}

external interface Client {
    @nativeInvoke
    operator fun invoke(path: String): ResponsePromise

    @nativeInvoke
    operator fun invoke(request: Request): ResponsePromise

    fun skip(): Client
    fun <T> wrap(interceptor: Interceptor<T>, config: T? = definedExternally /* null */): Client
}

external interface Meta {
    var client: Client
    var arguments: Any
}
