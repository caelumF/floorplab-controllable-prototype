@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION", "NESTED_CLASS_IN_EXTERNAL_INTERFACE")
@file:JsModule("rest/interceptor/location")
package com.amensio.floorplan_prototype.definitions.ksDef.rest

import kotlin.js.*

external interface Config {
    var client: Client? get() = definedExternally; set(value) = definedExternally
    var code: Number? get() = definedExternally; set(value) = definedExternally
}
