import org.jetbrains.kotlin.backend.common.atMostOne
import org.jetbrains.kotlin.gradle.tasks.Kotlin2JsCompile

group = "com.amensio"
version = "1.0-SNAPSHOT"
buildscript {
    var kotlin_version: String by extra
    kotlin_version = "1.2.10"

    repositories {
        mavenCentral()
    }

    dependencies {
        classpath(kotlinModule("gradle-plugin", kotlin_version))
    }
}

apply {
    plugin("java")
    plugin("kotlin2js")
}

val kotlin_version: String by extra

repositories {
    mavenCentral()
    jcenter()
}

dependencies {
    val kotlinxHtmlVersion = "0.6.3"
    val gsonVersion = "2.8.2"
    val kotsonVersion = "2.5.0"
    compile("org.jetbrains.kotlinx:kotlinx-html-js:$kotlinxHtmlVersion")
    compile("com.google.code.gson:gson:$gsonVersion")
    compile("com.github.salomonbrys.kotson:kotson:$kotsonVersion")
    compile(kotlinModule("stdlib-js", kotlin_version))
    testCompile("junit", "junit", "4.12")
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}

val mainSourceSet = the<JavaPluginConvention>().sourceSets["main"]!!
tasks {
    "compileKotlin2Js"(Kotlin2JsCompile::class) {
        kotlinOptions {
            outputFile = "$buildDir/web/output.js"
            sourceMap = true
        }
    }
    val unpackKotlinJsStdlib by creating {
        group = "build"
        description = "Unpack the Kotlin JavaScript standard library"
        val outputDir = file("$buildDir/$name")
        val compileClasspath = configurations["compileClasspath"]
        inputs.property("compileClasspath", compileClasspath)
        outputs.dir(outputDir)
        doLast {
            val kotlinStdLibJar = compileClasspath.single {
                it.name.matches(Regex("kotlin-stdlib-js-.+\\.jar"))
            }
            copy {
                includeEmptyDirs = false
                from(zipTree(kotlinStdLibJar))
                into(outputDir)
                include("**/*.js")
                exclude("META-INF/**")
            }
        }
    }
    val assembleWeb by creating(Copy::class) {
        group = "build"
        description = "Assemble the web application"
        includeEmptyDirs = false
        copy {
            from(unpackKotlinJsStdlib)
            from(mainSourceSet.output) {
                exclude("**/*.kjsm")
            }
            into("$buildDir/web")
        }
        //Web content
        copy {
            from("src/main/web")
            include("**")
            into("$buildDir/web")
        }
    }
    val updatePage by creating{

    }

    tasks.getByName("compileKotlin2Js").finalizedBy(tasks.getByName("assembleWeb"))
    "assemble" {
        dependsOn(assembleWeb)
    }
    "run web"{
        group = "dev"
        tasks.addAll(arrayOf(assembleWeb, project.getTasksByName("compileKotlin2Js", true).atMostOne()))
    }
}